import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Fadhlan Hafizh Permana, NPM 1706040132, Kelas B, GitLab Account: fadhlanhp
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		
		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjang = input.nextLine();
		int panjangs = Integer.parseInt(panjang);
		if (panjangs < 0 || panjangs > 250){
			System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		String lebar = input.nextLine();
		int lebars = Integer.parseInt(lebar);
		if (lebars < 0 || lebars > 250){
			System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggi = input.nextLine();
		int tinggis = Integer.parseInt(tinggi);
		if (tinggis < 0 || tinggis > 250){
			System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		String berat = input.nextLine();
		Float berats = Float.parseFloat(berat);
		if (berats < 0 || berats > 150){
			System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		String makanan = input.nextLine();
		int makanans = Integer.parseInt(makanan);
		if (makanans < 0 || makanans > 20){
			System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahCetakan = input.nextLine();
		int jumlahCetakans = Integer.parseInt(jumlahCetakan);

		if (catatan.isEmpty()) {
			catatan = "Tidak Ada Catatan Tambahan\n";
		}
		else {
			catatan = "Catatan: " + catatan + "\n";
		}
	
		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double rasio = berats / ((0.01 * panjangs) * (0.01 * lebars) * (0.01 * tinggis));

		for (int i=0;i<jumlahCetakans;i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + (i+1) + " dari " + jumlahCetakans + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			System.out.print("DATA SIAP DICETAK UNTUK " + penerima.toUpperCase() + "\n" +
							"--------------------\n" +
							nama + " - " + alamat + "\n" +
							"Lahir pada tanggal " + tanggalLahir + "\n" +
							"Rasio Berat Per Volume    = " + (int) rasio + " kg/m^3\n" +
							catatan);
		}

		
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)	
		char HurufPertama = nama.charAt(0);
		int jumlahascii = 0;
		
		for (int j=0; j<nama.length(); j++){
			if (nama != " "){
				jumlahascii = jumlahascii + (int) nama.charAt(j);
			}
		}
		int nomor = (((panjangs * lebars * tinggis) + jumlahascii) % 10000);
		

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = HurufPertama + Integer.toString(nomor);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		long anggaran = (50000 * 365 * makanans);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String tahunLahir = tanggalLahir.substring(6,10); // lihat hint jika bingung
		int tahunLahirs = Integer.parseInt(tahunLahir);
		int umur = 2018 - tahunLahirs;
		
		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaTempat = "";
		String namaKabupaten = "";
		if (umur < 19){
			namaTempat = "PPMT";
			namaKabupaten = "Rotunda";
		}
		else {
			if (anggaran <= 100000000){
				namaTempat = "Teksas";
				namaKabupaten = "Sastra";
			}
			else {
				namaTempat = "Mares";
				namaKabupaten = "Margonda";
			}
		}
		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		System.out.print("\nREKOMENDASI APARTEMEN\n" + 
						"--------------------\n" +
						"MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + "\n" +
						"MENIMBANG:  Anggaran makanan tahunan: Rp " + anggaran + "\n" +
						"            Umur kepala keluarga: " + umur + " tahun" + "\n" +
						"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:" + "\n" +
						namaTempat + "," + namaKabupaten);
		
		input.close();
	}
}