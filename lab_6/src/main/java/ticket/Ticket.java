package ticket;

import movie.Movie;
import theater.Theater;

public class Ticket {

    private Movie movie;
    private String date;
    private boolean is3D;

    public Ticket(Movie movie, String date, boolean is3D) {
        this.movie = movie;
        this.date = date;
        this.is3D = is3D;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isIs3D() {
        return is3D;
    }

    public void setIs3D(boolean is3D) {
        this.is3D = is3D;
    }

    public String check3D() {
        if (this.is3D) {
            return "3 Dimensi";
        } else {
            return "Biasa";
        }
    }

    public int ticketFee() {
        int weekdaysFee = 60000;
        if (this.date.equalsIgnoreCase("Sabtu") || this.date.equalsIgnoreCase("Minggu")) {
            int weekendsFee = weekdaysFee + 40000;
            if (!this.is3D) {
                return weekendsFee;
            } else {
                return (int)(1.2 * weekendsFee);
            }
        } else {
            if (!this.is3D) {
                return weekdaysFee;
            } else {
                return (int)(1.2 * weekdaysFee);
            }
        }
    }
}
