package customer;

import theater.Theater;
import ticket.Ticket;
import movie.Movie;

public class Customer {

    private String name;
    private int age;
    private String gender;

    public Customer(String name, String gender, int age) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String isGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Ticket orderTicket(Theater theater, String title, String date, String is3D) {
        boolean checkAvaibility = false;
        for (Ticket temp : theater.getTicketArrayList()) {
            boolean case1 = temp.getMovie().getTitle().equalsIgnoreCase(title);
            boolean case2 = temp.getDate().equalsIgnoreCase(date);
            boolean case3 = temp.check3D().equalsIgnoreCase(is3D);
            boolean case4 = this.getAge() >= temp.getMovie().checkUmur();

            if (case1 && case2 && case3) {
                if (case4) {
                    System.out.println(this.getName() + " telah membeli tiket " + title +
                            " jenis " + is3D + " di " + theater.getBuildingName() + " pada hari " +
                            date + " seharga Rp. " + temp.ticketFee());
                    theater.setBalance(theater.getBalance() + temp.ticketFee());
                    return temp;
                } else {
                    System.out.println(this.getName() + " masih belum cukup umur untuk menonton " +
                            title + " dengan rating " + temp.getMovie().getRating());
                    checkAvaibility = true;
                    break;
                }
            }
        }
        if (!checkAvaibility) {
            System.out.println("Tiket untuk film " + title + " jenis " + is3D +
                    " dengan jadwal " + date + " tidak tersedia di " +
                    theater.getBuildingName());
        }
        return null;
    }

    public void findMovie(Theater theater, String title) {
        boolean checkAvaibility = false;
        for (Ticket temp : theater.getTicketArrayList()) {
            boolean case5 = temp.getMovie().getTitle().equalsIgnoreCase(title);
            if (case5) {
                temp.getMovie().printMovies();
                checkAvaibility = true;
                break;
            }
        }
        if (!checkAvaibility) {
            System.out.println("Film " + title + " yang dicari " + this.getName() +
                    " tidak ada di bioskop " + theater.getBuildingName());
        }
    }
}
