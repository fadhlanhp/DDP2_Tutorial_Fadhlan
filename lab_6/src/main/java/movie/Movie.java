package movie;

public class Movie {

    private String title;
    private String genre;
    private int duration;
    private String rating;
    private String type;

    public Movie(String title, String rating, int duration, String genre, String type) {
        this.title = title;
        this.genre = genre;
        this.duration = duration;
        this.rating = rating;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int checkUmur() {
        if (this.getRating().equalsIgnoreCase("Remaja")) {
            return 13;
        } else if (this.getRating().equalsIgnoreCase("Dewasa")) {
            return 17;
        } else {
            return 0;
        }
    }

    public void printMovies() {
        System.out.println("------------------------------------------------------------------" + "\n" +
                        "Judul \t\t: " + getTitle() + "\n" +
                        "Genre \t\t: " + getGenre() + "\n" +
                        "Durasi \t\t: " + getDuration() + " menit\n" +
                        "Rating \t\t: " + getRating() + "\n" +
                        "Jenis \t\t: Film " + getType() + "\n" +
                "------------------------------------------------------------------");
    }
}
