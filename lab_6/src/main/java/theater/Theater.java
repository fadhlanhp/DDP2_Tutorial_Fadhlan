package theater;

import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;


public class Theater {

    private String buildingName;
    private int balance;
    private ArrayList<Ticket> ticketArrayList;
    private Movie[] moviesList;

    public Theater(String buildingName, int balance, ArrayList<Ticket> ticketArrayList, Movie[] moviesList) {
        this.buildingName = buildingName;
        this.balance = balance;
        this.ticketArrayList = ticketArrayList;
        this.moviesList = moviesList;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public ArrayList<Ticket> getTicketArrayList() {
        return ticketArrayList;
    }

    public void setTicketArrayList(ArrayList<Ticket> ticketArrayList) {
        this.ticketArrayList = ticketArrayList;
    }

    public Movie[] getMoviesList() {
        return moviesList;
    }

    public void setMoviesList(Movie[] moviesList) {
        this.moviesList = moviesList;
    }

    public void printInfo() {
        System.out.print("------------------------------------------------------------------" + "\n" +
                "Bioskop \t\t\t\t: " + getBuildingName() + "\n" +
                "Saldo Kas \t\t\t\t: " + getBalance() + "\n" +
                "Jumlah tiket tersedia \t: " + getTicketArrayList().size() + "\n" +
                "Daftar Film tersedia \t: "
        );
        int tempCounter = moviesList.length - 1;
        for (Movie temp : moviesList) {
            if (tempCounter > 0) {
                System.out.print(temp.getTitle() + ", ");
            } else {
                System.out.println(temp.getTitle());
            }
            tempCounter--;
        }
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theaters){
        int totalRevenueEarned = 0;
        for (Theater temp : theaters){
            totalRevenueEarned += temp.getBalance();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalRevenueEarned);
        System.out.println("------------------------------------------------------------------");
        for (Theater temp : theaters){
            System.out.println( "Bioskop \t\t: " + temp.getBuildingName() + "\n" +
                    "Saldo Kas \t\t: " + temp.getBalance() + "\n");
        }
        System.out.println("------------------------------------------------------------------");
    }
}
