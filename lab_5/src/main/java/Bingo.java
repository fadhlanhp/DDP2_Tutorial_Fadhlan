import java.util.Scanner;
public class Bingo {
    public static void main (String[] args){
        Scanner s = new Scanner(System.in);

        //Inisiasi varibel awal
        Number[][] temp = new Number[5][5];
        Number[] states = new Number[100];

        //For untuk bikin binggo cardnya
        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 5; j++) {
                String input = s.next();
                int value = Integer.parseInt(input);
                temp[i][j] = new Number(value, i, j);
                states[value] = temp[i][j];
            }
        }

        BingoCard Game = new BingoCard(temp, states);

        //Untuk commandnya
        while (true){
            String perintah = s.next();

            //Ketika perintahnya MARK
            if (perintah.equals("MARK")){
                int angka = s.nextInt();
                System.out.println(Game.markNum(angka));

                //Ngecek apakah sudah bingo
                if (Game.isBingo()){
                    System.out.println("BINGO!");
                    System.out.print(Game.info());
                    break;
                }
            }

            //Ketika perintahnya INFO
            else if (perintah.equals("INFO")){
                System.out.println(Game.info());
            }

            //Ketika perintahnya RESTART
            else if (perintah.equals("RESTART")){
                Game.restart();
            }

            //Ketika commandnya salah
            else {
                System.out.println ("Incorrect command");
            }

        }
    }
}