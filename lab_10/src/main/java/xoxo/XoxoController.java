package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.key.HugKey;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(actions -> encryptMessage());
        gui.setDecryptFunction(actions -> decryptMessage());
    }

    /**
     * Function to encrypt the message, and add to Log area
     */
    private void encryptMessage(){
        XoxoEncryption temp = new XoxoEncryption(gui.getKeyText());
        String hasil;
        if (gui.getSeedText().equalsIgnoreCase("DEFAULT_SEED")){
            hasil = temp.encrypt(gui.getMessageText(), HugKey.DEFAULT_SEED).getEncryptedMessage();
        }
        else if (gui.getSeedText().equalsIgnoreCase("")){
            hasil = temp.encrypt(gui.getMessageText()).getEncryptedMessage();
        }
        else {
            hasil = temp.encrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText())).getEncryptedMessage();
        }
        gui.appendLog(hasil);
        try {
            File file = new File("HasilEncrypt.enc");
            FileWriter writer = new FileWriter(file, true);
            writer.write(hasil + "\n");
            writer.flush();
        }
        catch (IOException e){
            JOptionPane.showMessageDialog(null, "File Error");
        }
    }

    /**
     * Function to decrypt the message, and add to Log area
     */
    private void decryptMessage(){
        XoxoDecryption tempDecrypt = new XoxoDecryption(gui.getKeyText());
        String hasil;
        if(gui.getSeedText().equalsIgnoreCase("DEFAULT_SEED")){
            hasil = tempDecrypt.decrypt(gui.getMessageText(), HugKey.DEFAULT_SEED);
        }
        else{
            hasil = tempDecrypt.decrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText()));
        }
        gui.appendLog(hasil);
        try {
            File file = new File("HasilDecrypt.txt");
            FileWriter writer = new FileWriter(file, true);
            writer.write(hasil + "\n");
            writer.flush();
            writer.close();
        }
        catch (IOException e){
            JOptionPane.showMessageDialog(null, "File Error");
        }
    }
}