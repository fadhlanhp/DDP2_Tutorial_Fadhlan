package xoxo.exceptions;

/**
 * An Exception that is thrown if message is bigger than 10Kbit
 *
 * @author Fadhlan Hafizh Permana
 */
public class SizeTooBigException extends RuntimeException{

    /**
     * Class Constructor
     */
    public SizeTooBigException (String message){
        super(message);
    }
}