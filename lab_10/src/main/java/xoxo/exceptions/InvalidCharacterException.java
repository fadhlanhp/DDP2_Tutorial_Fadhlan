package xoxo.exceptions;

/**
 * An exception that is thrown if String kiss key
 * include beside A-Z, a-z and @
 *
 * @author Fadhlan Hafizh Permana
 */
public class InvalidCharacterException extends RuntimeException {

    /**
     * Class Constructor
     */
    public InvalidCharacterException (String message){
        super(message);
    }
}