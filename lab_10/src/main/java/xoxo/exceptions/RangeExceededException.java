package xoxo.exceptions;

/**
 * An exception tha is thrown if the sedd isnt
 * between 0 and 36
 *
 * @author Fadhlan Hafizh Permana
 */
public class RangeExceededException extends RuntimeException {

    /**
     * Class Constructor
     */
    public RangeExceededException (String message){
        super(message);
    }
}