package xoxo;


import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;
/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Fadhlan Hafizh Permana
 */
public class XoxoView {

    private JFrame appFrame;
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A text that show beside message field
     */
    private JLabel messageText;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A text that show beside log field
     */
    private JLabel logText;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        appFrame = new JFrame("Translate Your Code!");
        JPanel upperPanel = new JPanel();
        upperPanel.setLayout(new GridLayout(1, 2));

        JPanel upperLeftPanel = new JPanel();
        upperLeftPanel.setLayout(new GridLayout(2, 1));
        messageField = new JTextField("Your Message", 10);
        messageField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
                if(messageField.getText().equals("Your Message")) {
                    messageField.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                if(messageField.getText().equals("")) {
                    messageField.setText("Your Message");
                }
            }
        });

        messageText = new JLabel("Massage:");

        upperLeftPanel.add(messageText);
        upperLeftPanel.add(messageField);

        JPanel upperRightPanel = new JPanel();
        upperRightPanel.setLayout(new GridLayout(2, 1));

        keyField = new JTextField("Key", 15);
        keyField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
                if(keyField.getText().equals("Key")) {
                    keyField.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                if(keyField.getText().equals("")) {
                    keyField.setText("Key");
                }
            }
        });

        seedField = new JTextField("DEFAULT_SEED", 15);
        seedField.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
                if(seedField.getText().equals("DEFAULT_SEED")) {
                    seedField.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                if(seedField.getText().equals("")) {
                    seedField.setText("DEFAULT_SEED");
                }
            }
        });

        upperRightPanel.add(keyField);
        upperRightPanel.add(seedField);


        upperPanel.add(upperLeftPanel);
        upperPanel.add(upperRightPanel);

        JPanel midPanel = new JPanel();

        encryptButton = new JButton("Encrypt");
        encryptButton.setBackground(Color.decode("#83bdbb"));
        decryptButton = new JButton("Decrypt");
        decryptButton.setBackground(Color.decode("#D96AE1"));

        midPanel.setLayout(new GridLayout(1, 2));
        midPanel.add(encryptButton);
        midPanel.add(decryptButton);

        JPanel midPanelAll = new JPanel();
        midPanelAll.setLayout(new GridLayout(2, 1));

        logText = new JLabel("Log:");

        midPanelAll.add(midPanel);
        midPanelAll.add(logText);
        logField = new JTextArea(10, 10);

        appFrame.add(upperPanel, BorderLayout.PAGE_START);
        appFrame.add(midPanelAll);
        appFrame.add(logField, BorderLayout.PAGE_END);
        appFrame.setSize(new Dimension(600, 300));
        appFrame.setVisible(true);
        appFrame.setResizable(false);
        appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}