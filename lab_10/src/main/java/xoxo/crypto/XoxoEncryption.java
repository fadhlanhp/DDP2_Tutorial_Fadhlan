package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

import javax.swing.*;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Fadhlan Hafizh Permana
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException{
        checkCharacter(kissKeyString);
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if ... <complete this>
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException {
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE){
            JOptionPane.showMessageDialog(null, "Seed is invalid!");
            throw new RangeExceededException("Seed is invalid");
        }
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if ... <complete this>
     * @throws InvalidCharacterException if ... <complete this>
     */
    private String encryptMessage(String message) throws SizeTooBigException {
        if (message.length() > 1250){
            JOptionPane.showMessageDialog(null, "Size of the message is too big!");
            throw new SizeTooBigException("Size of the message is too big!");
        }
        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }

    /**
     * to check Character in kiss key
     * @param kissKey kisskey that wanted to be check
     * @throws InvalidCharacterException valid character are a-z, A-Z, @
     */
    private void checkCharacter(String kissKey) throws InvalidCharacterException {
        for (int i = 0; i < kissKey.length() ; i++){
            if(!(Character.isLetter(kissKey.charAt(i)) || String.valueOf(kissKey.charAt(i)).equalsIgnoreCase("@"))){
                JOptionPane.showMessageDialog(null, "Message include an invalid character!");
                throw new InvalidCharacterException("Message include an invalid character!");
            }
        }
    }
}

