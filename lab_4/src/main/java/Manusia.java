public class Manusia {
	
	//Instance Variable
    private String nama;
    private int uang;
    private int umur;
    private float kebahagiaan;

	//Constructor ketika uang tidak ditentukan
    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.uang = 50000;
        this.kebahagiaan = 50;
    }
	
	//Constructor ketika uang awal ditentukan
    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = 50;
    }
	
	//Setter untuk variable nama
    public String getNama() {
        return nama;
    }
	
	//Getter untuk variable nama
    public void setNama(String nama) {
        this.nama = nama;
    }
	
	//Getter untuk variable uang
    public int getUang() {
        return uang;
    }
	
	//Setter untuk variable uang
    public void setUang(int uang) {
        this.uang = uang;
    }

	//Getter untuk variable umur
    public int getUmur() {
        return umur;
    }
	
	//Setter untuk variable umur
    public void setUmur(int umur) {
        this.umur = umur;
    }

	//Getter untuk variable kebahagiaan
    public float getKebahagiaan() {
        return kebahagiaan;
    }

	//Setter untuk variable kebahagiaan
    public void setKebahagiaan(float kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }

	//Method memberi uang jika jumlah uang tidak diketahui
    public void beriUang (Manusia penerima) {
        int jumlahascii = 0;
        for (int i = 0; i<penerima.getNama().length(); i++){
            jumlahascii = jumlahascii + (int) penerima.getNama().charAt(i);
        }
        int jumlahuang = jumlahascii * 100;
		
		//If jika uang tidak mencukupi
        if (this.uang < jumlahuang){
            System.out.println(this.nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
        }
        else {
			
			//Mengurangi jumlah uang yang dimiliki
			this.uang -= jumlahuang;
			
			//Menambah jumlah uang uang dimiliki
			penerima.setUang(penerima.getUang() + jumlahuang);
			
			//Mengecek apakah kebahagian melebihi batas atau tidak
            if (((this.kebahagiaan + (jumlahuang/6000)) <= 100)&&((penerima.getKebahagiaan() + (jumlahuang/6000)) <= 100)){
                this.kebahagiaan += ((float)jumlahuang)/6000;
                penerima.kebahagiaan = penerima.getKebahagiaan() + ((float) jumlahuang)/6000;
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            }
			
			//Jika kebahagiaan penerima melebihi batas
            else if ((this.kebahagiaan + (jumlahuang/6000)) <= 100){
                this.kebahagiaan += ((float)jumlahuang)/6000;
                penerima.kebahagiaan = 100;
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            }
			
			//Jika kebahagiaan pengirim melebihi batas
            else if (((penerima.getKebahagiaan() + (jumlahuang/6000) <= 100))){
                penerima.kebahagiaan += ((float)jumlahuang) / 6000;
                this.kebahagiaan = 100;
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
            }
			
			//Jika kebahagiaan pengirim dan penerima melebihi batas
			else {
				penerima.setKebahagiaan(100);
				this.kebahagiaan = 100;
				System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
			}
		}
    }

	//Method beriUang jika jumlah uang ditentukan
    public void beriUang (Manusia penerima, int jumlahuang) {
	
		//If jika uang yang ingin dikirimkan tidak cukup
        if (this.uang < jumlahuang){
            System.out.println(getNama() + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
        }
        else {
			
			//temp untuk kebahagiaan tambahan
			float temp = ((float) jumlahuang)/6000;
            
			//mengecek apakah kebahagiaan penerima dan pengirim melebihi batas
			if (((this.kebahagiaan + temp) > 100) && ((penerima.getKebahagiaan() + temp) > 100)){
				this.kebahagiaan = 100;
				penerima.setKebahagiaan(100);
				System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
			}
			
			//mengecek apakah hanya kebahagiaan pengirim yang melebihi batas
			else if ((this.kebahagiaan + temp) > 100) {
				penerima.kebahagiaan += temp;
                this.kebahagiaan = 100;
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
			}
			
			//mengecek apakah hanya kebahagian penerima yang melebihi batas
			else if ((penerima.getKebahagiaan() + temp) > 100){
				this.kebahagiaan += temp;
                penerima.setKebahagiaan(100);
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
			}
			
			//mengecek jika semua kebahagian baru tidak melebihi batas
			else {
				this.kebahagiaan += temp;
                penerima.kebahagiaan = penerima.getKebahagiaan() + temp;
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahuang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
			}
			
			//Mengurasi jumlah uang yang dimiliki
			this.uang -= jumlahuang;
			
			//Menambah jumlah yang yang dimiliki
			penerima.setUang(penerima.getUang() + jumlahuang);
        }
    }
	
	//Method Bekerja
    public void bekerja (float durasi, float bebanKerja){
		
		//Mengecek apakah seseorang cukup umur untuk bekerja
        if (this.umur < 18){
            System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
        }
        else {
			
			//Inisiasi Variabel Sementara yaitu BebanKerjaTotal dan pendapatan
            float BebanKerjaTotal = durasi * bebanKerja;
            float pendapatan;
			
			//Mengecek apakah Beban Kerja Total melebihi kebahagiaan yang dimiliki
            if (BebanKerjaTotal <= this.kebahagiaan){
                this.kebahagiaan -= BebanKerjaTotal;
                pendapatan = BebanKerjaTotal * 10000;
                System.out.println(this.nama + " bekerja full time, total pendapatan : " + (int) pendapatan);
            }
			
			//Jika Beban Kerja Total tidak melebihi kebahagiaan yang dimiliki
            else {
                int DurasiBaru = (int) (this.kebahagiaan / bebanKerja);
                BebanKerjaTotal = DurasiBaru * bebanKerja;
                this.kebahagiaan -= BebanKerjaTotal;
                pendapatan = BebanKerjaTotal * 10000;
                System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + (int) pendapatan);
            }
			
			//Menambahkan uang dengan pendapatan dari hasil bekerja
            this.uang += pendapatan;
        }
    }
	
	//Method rekreasi
    public void rekreasi (String namaTempat){
		
		//Menghitung biaya rekreasi
        int biaya = namaTempat.length() * 10000;
		
		//Mengecek apakah memiliki uang untuk berkreasi
        if (biaya >= this.uang){
            System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
        }
        else {
			
			//Mengecek apakah kebahagiaan melebihi batas
            if ((this.kebahagiaan + namaTempat.length()) <= 100){
                this.kebahagiaan += namaTempat.length();
            }
            else {
                this.kebahagiaan = 100;
            }
			
			//Mengurangi uang dengan biaya rekreasi
			this.uang -= biaya;
			
            System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
        }
    }

	//Method ketika manusia sedang sakit
    public void sakit (String namaPenyakit){
		
		//Mengecek apakah kebahagiaan akan sampai minus ketika sakit
        if ((this.kebahagiaan - namaPenyakit.length()) >= 0){
            this.kebahagiaan -= namaPenyakit.length();
        }
		
		//Ketika tidak punya kebahagiaan lagi karena sakit
        else {
            this.kebahagiaan = 0;
        }
		
        System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
    }
	
	//Nge-print Objek
    public String toString(){
        return ("Nama\t\t: " + this.nama + "\n" +
                "Umur\t\t: " + this.umur + "\n" +
                "Uang\t\t: " + (int) this.uang + "\n" +
                "Kebahagiaan\t: " + this.kebahagiaan);
    }
}

