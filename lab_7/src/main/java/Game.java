import character.*;
import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     *
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player temp : player) {
            if (temp.getName().equalsIgnoreCase(name)) {
                return temp;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (find(chara) == null) {
            if (tipe.equalsIgnoreCase("Human")) {
                Player pemain = new Human(chara, hp);
                player.add(0, pemain);
            } else if (tipe.equalsIgnoreCase("Magician")) {
                Player pemain = new Magician(chara, hp);
                player.add(0, pemain);
            } else {
                Player pemain = new Monster(chara, hp);
                player.add(0, pemain);
            }
            return (chara + " ditambah ke game");
        } else {
            return ("sudah ada karakter bernama " + chara);
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (find(chara) != null) {
            if (tipe.equalsIgnoreCase("Human")) {
                Player pemain = new Human(chara, hp);
                player.add(0, pemain);
            } else if (tipe.equalsIgnoreCase("Magician")) {
                Player pemain = new Magician(chara, hp);
                player.add(0, pemain);
            } else {
                Player pemain = new Monster(chara, hp, roar);
                player.add(0, pemain);
            }
            return (chara + " ditambah ke game");
        } else {
            return ("sudah ada karakter bernama " + chara);
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     *
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        if (find(chara) != null) {
            player.remove(find(chara));
            return (chara + " dihapus dari game");
        } else {
            return ("Tidak ada " + chara);
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     *
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        return find(chara).print();
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     *
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        String output = "";
        for (Player temp : player) {
            output += temp.print();
        }
        return output;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     *
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        String output = "";
        for (Player temp : find(chara).getDiet()) {
            output += temp.getType() + " " + temp.getName();
        }
        return output;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String output = "";
        for (Player temp : player) {
            for (Player temp1 : temp.getDiet())
                output += temp1.getType() + " " + temp1.getName();
        }
        return output.substring(0, output.length() - 2);
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        if ((find(meName) != null) && (find(enemyName) != null)) {
            return (find(meName).attack(find(enemyName)));
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        if ((find(meName) != null) && (find(enemyName) != null)) {
            return(find(meName).burn(find(enemyName)));
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        if ((find(meName) != null) && (find(enemyName) != null)) {
            if (find(meName).canEat(find(enemyName))) {
                find(meName).setHp(find(meName).getHp() + 15);
                find(meName).setDiet(find(enemyName));
                String output = meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + find(meName).getHp();
                remove(enemyName);
                return output;
            } else {
                return meName + " tidak bisa memakan " + enemyName;
            }
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        if (find(meName) != null) {
            if (find(meName).getType().equalsIgnoreCase("Monster")) {
                return(find(meName).roar());
            } else {
                return meName + " tidak bisa berteriak";
            }
        }
        else {
            return "Tidak ada " + meName;
        }
    }
}