package character;
public class Magician extends Human {

    public Magician(String name, int hp) {
        super(name, hp);
        this.setType("Magician");
    }

    public String burn(Player enemyName) {
        enemyName.setBurned(true);
        if (enemyName.getType().equalsIgnoreCase("Magician")) {
            enemyName.setHp(enemyName.getHp() - 20);
        } else {
            enemyName.setHp(enemyName.getHp() - 10);
        }
        if (enemyName.getHp() <= 0) {
            return "Nyawa " + enemyName.getName() + " " + enemyName.getHp() + "\n dan matang";
        }
        return "Nyawa " + enemyName.getName() + " " + enemyName.getHp();
    }
}
