package character;
import java.util.ArrayList;

public class Player {

    private String name;
    private boolean status; //True: Alive, False: Dead
    private boolean burned; //True: Burned, False: Not Burned
    private String type;
    private int hp;
    private ArrayList<Player> diet = new ArrayList<>();


    public Player(String name, int hp) {
        this.name = name;
        this.status = true;
        this.setHp(hp);
        this.burned = false;
    }

    public boolean isBurned() {
        return burned;
    }

    public void setBurned(boolean burned) {
        this.burned = burned;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setHp(int hp) {
        this.hp = hp;
        if (this.hp <= 0) {
            this.hp = 0;
            this.setStatus(false);
        }
    }

    public void setDiet(Player pemain) {
        this.diet.add(pemain);
    }

    public boolean canEat(Player pemain) {
        if (this.type.equalsIgnoreCase("Monster")) {
            return (!(pemain.getStatus()));
        } else {
            return (pemain.isBurned() && pemain.getType().equalsIgnoreCase("Monster"));
        }
    }

    public String roar() {
        return "";
    }

    public String burn(Player enemyName) {
        return "";
    }

    public String print() {
        String output = getType() + " " + getName() + "\n" +
                "HP: " + getHp() + "\n";
        if (getStatus()) {
            output += "Masih hidup\n";
        } else {
            output += "Sudah meninggal dunia dengan damai\n";
        }
        if (getDiet().size() == 0) {
            output += "Belum memakan siapa siapa";
        } else {
            output += "Memakan ";
            for (Player temp : getDiet()) {
                output += temp.getType() + " " + temp.getName();
            }
        }
        output += "\n";
        return output;
    }

    public String attack(Player enemyName) {
        if (enemyName != null) {
            if (enemyName.isBurned()) {
                enemyName.setHp(enemyName.getHp() - 20);
            } else {
                enemyName.setHp(enemyName.getHp() - 10);
            }
            return "Nyawa " + enemyName.getName() + " " + enemyName.getHp();
        } else {
            return "Tidak ada " + this.getName() + " atau " + enemyName.getName();
        }
    }
}
