package character;
public class Monster extends Player {

    private String roar;

    public Monster(String name, int hp) {
        super(name, hp);
        this.setHp(hp * 2);
        this.setType("Monster");
        this.roar = ("AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public Monster(String name, int hp, String roar) {
        super(name, hp);
        this.setHp(hp * 2);
        this.setType("Monster");
        this.roar = roar;
    }


    public String getRoar() {
        return roar;
    }

    public void setRoar(String roar) {
        this.roar = roar;
    }

    public String roar() {
        return this.roar;
    }
}
