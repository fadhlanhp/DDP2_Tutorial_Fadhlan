package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class representing event managing system
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }


    /**
     * Accessor for event field.
     *
     * @return Object of this event instance
     */
    public Event getEvent(String eventName) {
        for (Event temp : events) {
            if (temp.getName().equalsIgnoreCase(eventName)) {
                return temp;
            }
        }
        return null;
    }

    /**
     * Accessor for user field.
     *
     * @return object of this user instance
     */
    public User getUser(String userName) {
        for (User temp : users) {
            if (temp.getName().equalsIgnoreCase(userName)) {
                return temp;
            }
        }
        return null;
    }

    /**
     * Checking time whether the date is in wrong format
     *
     * @param startTimeStr start date of the event
     * @param endTimeStr   end date of the event
     * @return whether the date is in wrong format
     */
    private boolean checkTime(String startTimeStr, String endTimeStr) {
        try {
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date date1 = myFormat.parse(startTimeStr);
            Date date2 = myFormat.parse(endTimeStr);
            return date1.before(date2);
        } catch (ParseException e) {
            System.out.println("Tinggal yang dimasukkan salah");
        }
        return false;
    }

    /**
     * Create new event object and add them to event list
     *
     * @param name           name of event
     * @param startTimeStr   start date of event
     * @param endTimeStr     end date of event
     * @param costPerHourStr cost per house of event in String
     * @return a sentence to tell if the event has been successfully added
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        if (getEvent(name) == null && checkTime(startTimeStr, endTimeStr)) {
            Event newEvent = new Event(name, startTimeStr, endTimeStr, new BigInteger(costPerHourStr));
            events.add(newEvent);
            return "Event " + name + " berhasil ditambahkan!";
        } else if (getEvent(name) != null) {
            return "Event " + name + " sudah ada!";
        } else {
            return "Waktu yang diinputkan tidak valid!";
        }
    }

    /**
     * Create new user object and add to user list
     *
     * @param name name of the user
     * @return a sentence to tell if the user has been successfully added
     */
    public String addUser(String name) {
        if (getUser(name) == null) {
            User newUser = new User(name);
            users.add(newUser);
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
    }

    /**
     * register the event to user, adding event to user's event list
     * check if user and event is exist
     * check if event overlaps with other user's event
     *
     * @param userName  name of user object
     * @param eventName name of event object
     * @return a sentence to tell if the event has been successfully registered to user's event list
     */
    public String registerToEvent(String userName, String eventName) {
        if (getUser(userName) != null && getEvent(eventName) != null) {
            if (!getUser(userName).checkOverlaps(getEvent(eventName))) {
                getUser(userName).addEvent(getEvent(eventName));
                return userName + " berencana menghadiri " + eventName + "!";
            }
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        } else if (getUser(userName) == null && getEvent(eventName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (getUser(userName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else {
            return "Tidak ada acara dengan nama " + userName + "!";
        }

    }
}