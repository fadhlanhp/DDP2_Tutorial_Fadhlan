package lab9.user;

import lab9.event.Event;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.*;

/**
 * Class representing a user, willing to attend event(s)
 */
public class User {
    /**
     * Name of user
     */
    private String name;

    /**
     * List of events this user plans to attend
     */
    private ArrayList<Event> events;

    /**
     * Constructor
     * Initializes a user object with given name and empty event list
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
     * Accessor for name field
     *
     * @return name of this instance
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a new event to this user's planned events, if not overlapping
     * with currently planned events.
     *
     * @return true if the event if successfully added, false otherwise
     */
    public boolean addEvent(Event newEvent) {
        if (!checkOverlaps(newEvent)) {
            events.add(newEvent);
            return true;
        }
        return false;
    }

    /**
     * Returns the list of events this user plans to attend,
     * Sorted by their starting time.
     * Note: The list returned from this method is a copy of the actual
     * events field, to avoid mutation from external sources
     *
     * @return list of events this user plans to attend
     */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> newList = new ArrayList<>(events);
        Collections.sort(newList, new DateComparator());

        return newList;
    }

    /**
     * To check whether newEvent overlaps with other user's event
     *
     * @param newEvent object of newEvent
     * @return check if newEvent overlaps with other user's event
     */
    public boolean checkOverlaps(Event newEvent) {
        for (Event temp : events) {
            if (temp.overlapsWith(newEvent)) {
                return true;
            }
        }
        return false;
    }

    /**
     * count total cost of all user's cost event
     *
     * @return total cost of user's event
     */
    public BigInteger getTotalCost() {
        BigInteger allCost = new BigInteger("0");
        for (Event temp : getEvents()) {
            allCost = allCost.add(temp.getCost());
        }
        return allCost;
    }

}

/**
 * Class to help sort the event list
 */
class DateComparator implements Comparator<Event> {

    @Override
    public int compare(Event o1, Event o2) {
        return o1.getStart().compareTo(o2.getStart());
    }
}
