package lab9.event;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A class representing an event and its properties
 */
public class Event {
    /**
     * Name of event
     */
    private String name;

    /**
     * Start date of event
     */
    private Date start;

    /**
     * End date of event
     */
    private Date end;

    /**
     * Cost of event
     */
    private BigInteger cost;

    /**
     * Constructor. Initializes events with their name, start date, end date and cost.
     */
    public Event(String name, String start, String end, BigInteger cost) {
        try {
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date date1 = myFormat.parse(start);
            Date date2 = myFormat.parse(end);
            this.name = name;
            this.start = date1;
            this.end = date2;
            this.cost = cost;

        } catch (ParseException e) {
            System.out.println("Tanggal yang dimasukkan salah");
        }
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for start date field.
     *
     * @return start date of this event instance
     */
    public Date getStart() {
        return start;
    }

    /**
     * Accessor for end date field.
     *
     * @return end date of this event instance
     */
    private Date getEnd() {
        return end;
    }

    /**
     * Accessor for cost field.
     *
     * @return cost of this event instance
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * Accessor for event field.
     *
     * @return name, start date, end date and cost of this event instance
     */
    public String toString() {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return getName() + "\n" +
                "Waktu mulai: " + myFormat.format(getStart()) + "\n" +
                "Waktu selesai: " + myFormat.format(getEnd()) + "\n" +
                "Biaya kehadiran: " + cost;
    }

    /**
     * Check between two event wheter they are overlaps with each other
     *
     * @param other other event you want to compare with
     * @return boolean, if one event overlaps with each other
     */
    public boolean overlapsWith(Event other) {
        boolean first = this.getStart().after(other.getStart()) && this.getStart().before(other.getEnd());
        boolean second = this.getEnd().after(other.getStart()) && this.getEnd().before(other.getEnd());
        return (first || second);
    }
}
