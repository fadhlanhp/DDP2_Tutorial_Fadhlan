public class Staff extends Manager {

    /**
     * Constructor untuk Staff
     * @param name      Nama Karyawannya
     * @param salary    Gaji dari Karyawannya
     */
    public Staff(String name, int salary) {
        super(name, salary);
        this.type = "Staff";
    }
}
