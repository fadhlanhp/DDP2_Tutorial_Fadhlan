import java.util.ArrayList;

public class Manager extends Karyawan {

    /**
     * Constructor
     * @param name      Nama dari karyawannya
     * @param salary    Gaji dari karyawannya
     */
    public Manager(String name, int salary) {
        super(name, salary);
        this.type = "Manager";
    }

    /**
     * Constructor overloading
     * @param name          Nama dari kayawannya
     * @param salary        Gaji dari karyawannya
     * @param gajianKe      Karyawan tersebut sedang gajian ke berapa
     * @param totalSalary   Total uang yang telah ia dapatkan
     * @param listK         Listbawahan dia
     */
    public Manager(String name, int salary, int gajianKe, int totalSalary, ArrayList<Karyawan> listK) {
        super(name, salary, gajianKe, totalSalary, listK);
    }

}
