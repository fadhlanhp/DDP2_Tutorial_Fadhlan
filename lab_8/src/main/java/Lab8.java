/**
 * Tugas Lab8 membuat sebuah kodingan untuk sebuah kantor
 * @author Fadhlan Hafizh Permana - 1706040132 - DDP 2 Kelas B
 */

import java.util.ArrayList;
import java.util.Scanner;


public class Lab8 {
    private static ArrayList<Karyawan> listKaryawan = new ArrayList<>();


    /**
     * Method untuk mencari sebuah object didalam arraylist ListKaryawan
     * @param name nama yang akan dicari
     * @return Object Karyawan yang ketemu atau null kalau ga ketemu
     */
    public static Karyawan find(String name) {
        for (Karyawan temp : listKaryawan) {
            if (temp.getName().equalsIgnoreCase(name)) {
                return temp;
            }
        }
        return null;
    }

    /**
     * Method untuk dia naik pangkat, di method ini dia inisasi object Manager baru dan meremove object Karyawan yang lama di arraylist
     * @param karyawan Karyawan yang akan dinaikkan pangkatnya
     */
    public static void naikPangkat(Karyawan karyawan) {
        if (karyawan.getType().equalsIgnoreCase("Staff")) {
            String name = karyawan.getName();
            int salary = karyawan.getSalary();
            int gajianKe = karyawan.getGajianKe();
            int totalSalary = karyawan.getTotalSalary();
            ArrayList<Karyawan> listBawahan = karyawan.getListBawahan();
            Karyawan newManager = new Manager(name, salary, gajianKe, totalSalary, listBawahan);
            listKaryawan.remove(karyawan);
            listKaryawan.add(newManager);
            System.out.println("Selamat, " + name + " telah dipromosikan menjadi MANAGER");
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int gajiDefault = Integer.parseInt(input.nextLine());

        //Looping agar input terus menerus
        while (true) {
            String[] tempInput = input.nextLine().split(" ");
            if (tempInput[0].equalsIgnoreCase("TAMBAH_KARYAWAN")) {
                String name = tempInput[1];
                if (find(name) != null) {
                    System.out.println("Karyawan dengan nama " + name + " telah terdaftar");
                } else {
                    int salary = Integer.parseInt(tempInput[3]);
                    if (tempInput[2].equalsIgnoreCase("MANAGER")) {
                        Karyawan karyawan = new Manager(name, salary);
                    } else if (tempInput[2].equalsIgnoreCase("STAFF")) {
                        Karyawan karyawan = new Staff(name, salary);
                        listKaryawan.add(karyawan);
                    } else if (tempInput[2].equalsIgnoreCase("INTERN")) {
                        Karyawan karyawan = new Intern(name, salary);
                        listKaryawan.add(karyawan);
                    }
                    System.out.println(name + " mulai bekerja sebagai " + tempInput[2] + " di PT. TAMPAN");
                }
            } else if (tempInput[0].equalsIgnoreCase("TAMBAH_BAWAHAN")) {
                String atasan = tempInput[1];
                String bawahan = tempInput[2];

                if (find(atasan) == null || find(bawahan) == null) {
                    System.out.println("Nama tidak berhasil ditemukan");
                } else {
                    if (find(atasan).getType().equalsIgnoreCase("Manager")) {
                        if (!find(bawahan).getType().equalsIgnoreCase("Manager")) {
                            if (find(atasan).getListBawahan().contains(find(bawahan))) {
                                System.out.println("Karyawan " + atasan + " telah menjadi bawahan " + bawahan);
                            } else {
                                find(atasan).addListBawahan(find(bawahan));
                                System.out.println("Karyawan " + bawahan + " berhasil ditambahkan menjadi bawahan " + atasan);
                            }
                        } else {
                            System.out.println("Anda tidak layak memiliki bawahan");
                        }
                    } else if (find(atasan).getType().equalsIgnoreCase("Staff")) {
                        if (find(bawahan).getType().equalsIgnoreCase("Intern")) {
                            if (find(atasan).getListBawahan().contains(find(bawahan))) {
                                System.out.println("Karyawan " + atasan + " telah menjadi bawahan " + bawahan);
                            } else {
                                find(atasan).addListBawahan(find(bawahan));
                                System.out.println("Karyawan " + bawahan + " berhasil ditambahkan menjadi bawahan " + atasan);
                            }
                        } else {
                            System.out.println("Anda tidak layak memiliki bawahan");
                        }
                    } else {
                        System.out.println("Anda tidak layak memiliki bawahan");
                    }
                }


            } else if (tempInput[0].equalsIgnoreCase("STATUS")) {
                String name = tempInput[1];
                if (find(name) == null) {
                    System.out.println("Karyawan tidak ditemukan");
                } else {
                    System.out.println(name + " " + find(name).getSalary());
                }

            } else if (tempInput[0].equalsIgnoreCase("GAJIAN")) {
                ArrayList<Karyawan> listNaikPangkat = new ArrayList<>();
                for (Karyawan temp : listKaryawan) {
                    temp.setTotalSalary(temp.getTotalSalary() + temp.getSalary());
                    temp.setGajianKe(temp.getGajianKe() + 1);
                    if (temp.getGajianKe() == 6) {
                        temp.setGajianKe(0);
                        int newSalary = temp.getSalary() + (temp.getSalary() / 10);
                        System.out.println(temp.getName() + " mengalami kenaikan gaji sebesar 10% dari " +
                                temp.getSalary() + " menjadi " + newSalary);
                        temp.setSalary(newSalary);
                    }
                    if (temp.getSalary() >= gajiDefault && (temp instanceof Staff)) {
                        listNaikPangkat.add(temp);
                    }
                }
                System.out.println("Semua karyawan telah diberikan gaji");
                if (listNaikPangkat.size() > 0) {
                    for (Karyawan temp2 : listNaikPangkat) {
                        naikPangkat(temp2);
                    }
                }
            }
        }
    }
}

