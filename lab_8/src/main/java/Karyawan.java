import java.util.ArrayList;

abstract class Karyawan {
    protected String name;
    protected int salary;
    protected ArrayList<Karyawan> listBawahan = new ArrayList<>();
    protected int gajianKe;
    protected String type;
    protected int totalSalary;

    /**
     * Contructor untuk membuat object Karyawan
     * @param name      nama dari Karyawannya
     * @param salary    salary dari Karyawannya
     */
    public Karyawan(String name, int salary) {
        this.name = name;
        this.salary = salary;
        this.gajianKe = 0;
        this.totalSalary = 0;
    }

    /**
     * Overloading consructor, ini dipake buat kalau dia naik pangkat
     * @param name          Nama dari karyawannya
     * @param salary        Gaji dari kawyawannya
     * @param gajianKe      Gajian kebeerapa dari karyawannya
     * @param totalSalary   Total uang yang ia miliki
     * @param listK         list bawahan yang ia punya
     */
    public Karyawan(String name, int salary, int gajianKe, int totalSalary, ArrayList<Karyawan> listK) {
        this.name = name;
        this.salary = salary;
        this.gajianKe = gajianKe;
        this.totalSalary = totalSalary;
        this.listBawahan = listK;
    }

    public int getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(int totalSalary) {
        this.totalSalary = totalSalary;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public ArrayList<Karyawan> getListBawahan() {
        return listBawahan;
    }

    /**
     * untuk nambah bawahannya Karyawan
     * @param bawahan   Object karyawan yang akan ditambahkan menjadi sebuah bawahan
     */
    public void addListBawahan(Karyawan bawahan) {
        if (this.listBawahan.size() <= 10) {
            this.listBawahan.add(bawahan);
        }
        else {
            System.out.println("Sudah penuh");
        }
    }

    public int getGajianKe() {
        return gajianKe;
    }

    public void setGajianKe(int gajianKe) {
        this.gajianKe = gajianKe;
    }

    public String getType() {
        return type;
    }
}
