public class Intern extends Staff {

    /**
     * Constructor untuk Intern
     * @param name      Nama dari Karyawannya
     * @param salary    Gaji dari karywannya
     */
    public Intern(String name, int salary) {
        super(name, salary);
        this.type = "Intern";
    }
}
