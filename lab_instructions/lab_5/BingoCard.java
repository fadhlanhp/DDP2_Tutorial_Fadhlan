public class BingoCard {

    //Inisiasi Instance Variable
    private Number[][] numbers;
    private Number[] numberStates;
    private boolean isBingo;

    //Contstructor BingoCard
    public BingoCard(Number[][] numbers, Number[] numberStates) {
        this.numbers = numbers;
        this.numberStates = numberStates;
        this.isBingo = false;
    }

    //getter variabel array Numbers
    public Number[][] getNumbers() {
        return numbers;
    }

    //setter variabel array Numbers
    public void setNumbers(Number[][] numbers) {
        this.numbers = numbers;
    }

    //Getter variabel array NumberStates
    public Number[] getNumberStates() {
        return numberStates;
    }

    //Setter Variabel array NumberStates
    public void setNumberStates(Number[] numberStates) {
        this.numberStates = numberStates;
    }

    //Ambil isBingo
    public boolean isBingo() {
        return isBingo;
    }

    //Setter isBingo
    public void setBingo(boolean isBingo) {
        this.isBingo = isBingo;
    }

    //Methos MarkNum
    public String markNum(int num){

        //Ngecek apakah ada object di arraynya
        if (this.numberStates[num] == null){
            return ("Kartu tidak memiliki angka " + num);
        }
        else {

            //Ngecek apakah udah ke check obejctnya
            if (this.numberStates[num].isChecked()){
                return (num + " sebelumnya sudah tersilang");
            }
            else {
                this.numberStates[num].setChecked(true);
                this.check();
                return (num + " tersilang");
            }
        }
    }

    //Method Info
    public String info(){

        //Inisiasi output awal
        String output = "";

        //For untuk ngeprint bingocardnya
        for (int i = 0; i<5 ; i++){
            for (int j = 0; j<5; j++){

                //ngecek apakah nomornya ke "check"
                if (this.numbers[i][j].isChecked()){
                    output += "| X  ";
                }
                else{
                    output += "| " + this.numbers[i][j].getValue() + " ";
                }
            }
            //Agar di baris terakhir gaada baris kosong tambahan
            if (i<4){
                output += "|\n";
            }
            if (i == 4){
                output += "|";
            }
        }
        return output;
    }

    //Merestart bingocardnya
    public void restart(){
        for (int i = 0; i<5; i++){
            for (int j = 0; j<5; j++){
                this.numbers[i][j].setChecked(false);
            }
        }
        System.out.println("Mulligan!");
    }

    //Ngecheck apakah sudah bingo atau belum
    public void check () {

        //Vertikal
        for (int i = 0; i<5; i++){
            if (this.numbers[i][0].isChecked() && this.numbers[i][1].isChecked() && this.numbers[i][2].isChecked() && this.numbers[i][3].isChecked() && this.numbers[i][4].isChecked()){
                this.setBingo(true);
            }
        }

        //Horizontal
        for (int j = 0; j<5; j++){
            if (this.numbers[0][j].isChecked() && this.numbers[1][j].isChecked() && this.numbers[2][j].isChecked() && this.numbers[3][j].isChecked() && this.numbers[4][j].isChecked()){
                this.setBingo(true);
            }
        }

        //Dioagonal
        if (this.numbers[1][1].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked() && this.numbers[4][4].isChecked() && this.numbers[0][0].isChecked()){
            this.setBingo(true);
        }

        //Dioagonal
        if (this.numbers[0][4].isChecked() && this.numbers[1][3].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][1].isChecked() && this.numbers[4][0].isChecked()){
            this.setBingo(true);
        }
    }
}
