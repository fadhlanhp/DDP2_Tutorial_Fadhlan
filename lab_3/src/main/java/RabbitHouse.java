import java.util.Scanner;
public class RabbitHouse {
	static int jumlahanaknya = 0;
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String jenis = input.next();
		String nama = input.next();
		int jumlahhuruf = nama.length();
		String inputan = input.nextLine();
		//int jumlahhuruf = inputan.split(" ")[1].length();
		RabbitHouse HitungAnak = new RabbitHouse();
		if (jenis.equals("normal")) {
			System.out.println(HitungAnak.factorial(jumlahhuruf, 1));
		}
		else if(jenis.equals("palindrom")) {
			if (HitungAnak.cekpalindrom(nama) == true){
				System.out.println(0);
			}	
			else{
				jumlahanaknya++;
				System.out.println(HitungAnak.bikinanak(nama));
			}
		input.close();
		}
	}
	
	public static int factorial (int angka, int jumlah){
		if (angka == 1){
			return 1;
		}
		else {
			return (angka*jumlah) + factorial(angka - 1, (angka*jumlah));
		}
	}
		
	public static boolean cekpalindrom (String nama){
		int posisiawal = 0;
		int posisiakhir = nama.length() - 1;
		for (int i = 0; i<nama.length(); i++){
			if (nama.charAt(posisiawal) != nama.charAt(posisiakhir)){
				posisiawal++;
				posisiakhir--;
				return false;
			}
			else {
				posisiawal++;
				posisiakhir--;
				continue;
			}
		}
		return true;
	}
	
			
	public static int bikinanak (String namabapak){
		if (namabapak.length() == 0){
			 jumlahanaknya += 0;
		}
		else {
			for (int j = 0; j<namabapak.length(); j++){
				String namaanak = namabapak.substring(0, j) + namabapak.substring(j + 1, namabapak.length());
				if (namaanak.length() == 0){
					continue;
				}
				if (cekpalindrom(namaanak) == true){
					continue;
				}
				else{
					jumlahanaknya += 1;
					bikinanak(namaanak);
				}
			}
		}
		return jumlahanaknya;
	}
}